<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Admin\Http\Controllers'], function()
{

	define('TPL_NAME', TPL_ADMIN_NAME);
	define('ADMIN_TEMPLATES_URL', ADMIN_SITE_URL . '/templates/' . TPL_NAME);
	define('BASE_TPL_PATH', BASE_PATH . '/templates/' . TPL_NAME);

	Base::run();

	Route::get('/', 'AdminController@index');
	Route::get('/test', 'TestController@index');
});